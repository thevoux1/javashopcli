package pl.pb.shop_cli.controller;

import pl.pb.shop_cli.Credentials;
import pl.pb.shop_cli.model.Product;
import pl.pb.shop_cli.model.ProductAmount;
import pl.pb.shop_cli.model.ProductBasket;
import pl.pb.shop_cli.service.BasketService;
import pl.pb.shop_cli.service.ProductService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Named
@ViewScoped
public class ProductController implements Serializable {
    @EJB
    private ProductService productService;
    @EJB
    private BasketService basketService;
    private List<Product> products;
    private Product editedProduct;

    public List<Product> getProducts() {
        return products;
    }

    public Product getEditedProduct() {
        return editedProduct;
    }

    @PostConstruct
    private void findAll() {
        products = productService.findAll();
    }

    public void onAddProduct() {
        editedProduct = new Product();
    }

    public void onSaveProduct() {
        Product newProduct = productService.save(editedProduct, Credentials.Token);

        if(editedProduct.getId() == null)
            products.add(newProduct);
        else
            products.replaceAll(p -> p != editedProduct ? p : newProduct);

        editedProduct = null;
    }

    public void onCancelProduct() {
        editedProduct = null;
    }

    public void onEditProduct(Product product) {
        editedProduct = product;
    }

    public void onRemoveProduct(Product product) {
        productService.remove(product, Credentials.Token);
        products.remove(product);
    }

    public void onAddToBasket(Product product) {
        if (product.getAmount() < 1)
            return;

        Optional<ProductBasket> productBasket = basketService.getBasket(Credentials.Token).getProductBaskets()
                .stream().filter(x -> x.getProduct().getId().equals(product.getId())).findAny();

        Response response;
        if (productBasket.isPresent())
            response = basketService.changeProductAmount(new ProductAmount(product.getId(), productBasket.get().getAmount() + 1), Credentials.Token);
        else
            response = basketService.addProduct(new ProductAmount(product.getId(), 1L), Credentials.Token);

        if (response.getStatusInfo().equals(Response.Status.NO_CONTENT))
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Dodano do koszyka", "Pomyślnie dodano produkt \"" + product.getName() + "\" do koszyka"));
        else
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Błąd", "Nie udało się dodać produktu \"" + product.getName() + "\" do koszyka"));
    }
}
