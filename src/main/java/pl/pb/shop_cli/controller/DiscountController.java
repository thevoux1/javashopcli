package pl.pb.shop_cli.controller;

import pl.pb.shop_cli.Credentials;
import pl.pb.shop_cli.model.Discount;
import pl.pb.shop_cli.service.DiscountService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class DiscountController implements Serializable {
    @EJB
    private DiscountService discountService;
    private List<Discount> discounts;
    private Discount editedDiscount;
    private byte previousPercent;

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public Discount getEditedDiscount() {
        return editedDiscount;
    }

    @PostConstruct
    private void findAll() {
        discounts = discountService.findAll(Credentials.Token);
    }

    public void onEditDiscount(Discount discount) {
        editedDiscount = discount;
        previousPercent = editedDiscount.getPercent();
    }

    public void onRemoveDiscount(Discount discount) {
        discounts.remove(discount);
        discountService.remove(discount, Credentials.Token);
    }

    public void onAddDiscount() {
        editedDiscount = new Discount();
    }

    public void onSaveDiscount() {
        if(editedDiscount.getPercent() <= 0 || editedDiscount.getPercent() >= 100) {
            editedDiscount.setPercent(previousPercent);
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage("amountError", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Procent rabatu musi być większy od 0 i mniejszy od 100"));
            return;
        }

        Discount newDiscount = discountService.save(editedDiscount, Credentials.Token);
        if(newDiscount == null) {
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Zniżka z takim kodem już istnieje"));
            return;
        }

        if(editedDiscount.getId() == null)
            discounts.add(newDiscount);
        else
            discounts.replaceAll(p -> p != editedDiscount ? p : newDiscount);

        editedDiscount = null;
        FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Dodany zniżkę",
                "Pomyślnie dodano kod rabatowy"));
    }

    public void onCancelDiscount() {
        editedDiscount = null;
    }
}
