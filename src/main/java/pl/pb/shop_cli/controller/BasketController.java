package pl.pb.shop_cli.controller;

import pl.pb.shop_cli.Credentials;
import pl.pb.shop_cli.model.*;
import pl.pb.shop_cli.service.BasketService;
import pl.pb.shop_cli.service.OrderService;
import pl.pb.shop_cli.service.ProductService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;

import static pl.pb.shop_cli.Constants.APP_NAME;

@Named
@ViewScoped
public class BasketController implements Serializable {
    @EJB
    private BasketService basketService;
    @EJB
    private ProductService productService;
    @EJB
    private OrderService orderService;
    private Basket basket;
    private ProductBasket editedProductBasket;
    private Long previousAmount;
    private String discountCode;

    public Basket getBasket() {
        return basket;
    }

    public ProductBasket getEditedProductBasket() {
        return editedProductBasket;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public boolean isBasketEmpty() {
        return basket.getProductBaskets().size() < 1;
    }

    public BigDecimal getTotalBasketPrice() {
        BigDecimal totalPrice = new BigDecimal(0);
        for(ProductBasket product : basket.getProductBaskets())
            totalPrice = totalPrice.add(product.getTotalPrice());

        return totalPrice;
    }

    public BigDecimal getDiscountSize() {
        return getTotalBasketPrice().multiply(new BigDecimal(basket.getDiscount().getPercent())).divide(new BigDecimal(100));
    }

    public BigDecimal getTotalBasketPriceWithDiscount() {
        return getTotalBasketPrice().subtract(getDiscountSize());
    }

    @PostConstruct
    public void initBasket() {
        basket = basketService.getBasket(Credentials.Token);
    }

    public void onRemoveProduct(ProductBasket productBasket) {
        basket.getProductBaskets().remove(productBasket);
        basketService.removeProduct(productBasket.getProduct().getId(), Credentials.Token);
        Product product = productBasket.getProduct();
        product.setAmount(product.getAmount() + productBasket.getAmount());
        productService.save(product, Credentials.Token);
    }

    public void onClearBasket() {
        for(ProductBasket productBasket : basket.getProductBaskets()) {
            Product product = productBasket.getProduct();
            product.setAmount(product.getAmount() + productBasket.getAmount());
            productService.save(product, Credentials.Token);
        }

        basketService.clearBasket(Credentials.Token);
        basket.clearBasket();
    }

    public void onEditProduct(ProductBasket productBasket) {
        editedProductBasket = productBasket;
        previousAmount = productBasket.getAmount();
    }

    public void onSaveEdit() {
        if(editedProductBasket.getAmount() < 1) {
            editedProductBasket.setAmount(previousAmount);
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage("amountError", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Liczba produktów nie może być mniejsza od jeden!"));
            return;
        }

        if(previousAmount != editedProductBasket.getAmount()) {
            Response response = basketService.changeProductAmount(
                    new ProductAmount(editedProductBasket.getProduct().getId(), editedProductBasket.getAmount()), Credentials.Token);

            if(response.getStatusInfo().equals(Response.Status.NO_CONTENT)) {
                FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Zaktualizowano koszyk",
                        "Pomyślnie zaktualizowano zawartość koszyka"));
            } else {
                editedProductBasket.setAmount(previousAmount);
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage("amountError", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                        "Niewystraczająca liczba produktów na stanie!"));
                return;
            }
        }

        editedProductBasket = null;
    }

    public void onCancelEdit() {
        editedProductBasket = null;
    }

    public void onApplyDiscount() {
        Response response = basketService.addDiscount(discountCode, Credentials.Token);
        if(response.getStatus() != 400) {
            initBasket();
        } else {
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage("discountError", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Nieprawidłowy kod rabatowy"));
        }
    }

    public void onRemoveDiscount() {
        basket.setDiscount(null);
        basketService.removeDiscount(Credentials.Token);
    }

    public void onSubmitOrder() throws IOException {
        Response response = orderService.submitOrder(Credentials.Token);
        if(response.getStatusInfo().equals(Response.Status.NO_CONTENT))
            FacesContext.getCurrentInstance().getExternalContext().redirect(APP_NAME + "pages/my-orders.xhtml");
        else
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Podczas składania zamówienia wystąpił błąd"));
    }
}
