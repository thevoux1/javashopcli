package pl.pb.shop_cli.controller;

import pl.pb.shop_cli.Credentials;
import pl.pb.shop_cli.model.OrderState;
import pl.pb.shop_cli.model.ShopOrder;
import pl.pb.shop_cli.model.User;
import pl.pb.shop_cli.service.OrderService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class OrderController implements Serializable {
    @EJB
    private OrderService orderService;
    private List<ShopOrder> allOrders;
    private List<ShopOrder> myOrders;
    private ShopOrder selectedOrder;

    public List<ShopOrder> getAllOrders() {
        return allOrders;
    }

    public List<ShopOrder> getMyOrders() {
        return myOrders;
    }

    public ShopOrder getSelectedOrder() {
        return selectedOrder;
    }

    public OrderState[] getOrderStates() {
        return OrderState.values();
    }

    @PostConstruct
    public void findOrders() {
        allOrders = orderService.findAllOrders(Credentials.Token);
        myOrders = orderService.findMyOrders(Credentials.Token);
    }

    public void onShowProducts(ShopOrder order) {
        selectedOrder = order;
    }

    public void onCloseProducts() {
        selectedOrder = null;
    }

    public void onPayOrder(ShopOrder order) {
        Response response = orderService.payOrder(order, Credentials.Token);
        if(response.getStatusInfo().equals(Response.Status.NO_CONTENT)) {
            order.setOrderState(OrderState.PREPARING);
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Opłacono zamówienie",
                    "Pomyślnie opłacono zamówienie #" + order.getId()));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Błąd podczas opłacania zamówienia #" + order.getId()));
        }
    }

    public void onCancelOrder(ShopOrder order) {
        Response response = orderService.setOrderState(order, OrderState.CANCALLED, Credentials.Token);
        if(response.getStatusInfo().equals(Response.Status.NO_CONTENT)) {
            order.setOrderState(OrderState.CANCALLED);
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Anulowano zamówienie",
                    "Pomyślnie anulowano zamówienie #" + order.getId()));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Błąd podczas anulowania zamówienia #" + order.getId()));
        }
    }

    public void onStatusChanged(ValueChangeEvent event) {
        ShopOrder order = (ShopOrder) ((UIInput) event.getSource()).getAttributes().get("order");
        Response response = orderService.setOrderState(order, (OrderState) event.getNewValue(), Credentials.Token);
        if(response.getStatusInfo().equals(Response.Status.NO_CONTENT)) {
            order.setOrderState(OrderState.CANCALLED);
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Zmiana status zamówienia",
                    "Pomyślnie zmieniono status zamówienia #" + order.getId()));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Błąd",
                    "Błąd zmiany statusu zamówienia #" + order.getId()));
        }
    }
}
