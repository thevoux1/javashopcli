package pl.pb.shop_cli.controller;

import pl.pb.shop_cli.Credentials;
import pl.pb.shop_cli.model.RegisterUser;
import pl.pb.shop_cli.model.User;
import pl.pb.shop_cli.model.UserRole;
import pl.pb.shop_cli.service.UserService;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;

import static pl.pb.shop_cli.Constants.APP_NAME;

@Named
@ViewScoped
public class UserController implements Serializable {
    @EJB
    private UserService userService;
    private RegisterUser registerUser;

    public User getCurrentUser() {
        return Credentials.CurrentUser;
    }

    public RegisterUser getRegisterUser() {
        return registerUser;
    }

    public boolean isLogged() {
        return Credentials.CurrentUser != null;
    }

    public boolean isAdmin() {
        if(Credentials.CurrentUser == null || Credentials.CurrentUser.getUserRole() != UserRole.ADMIN)
            return false;

        return true;
    }

    public void onLogin() {
        Credentials.CurrentUser = new User();
    }

    public void onRegister() {
        registerUser = new RegisterUser();
        registerUser.setUserRole(UserRole.CLIENT);
    }

    public void onLogout() throws IOException {
        Credentials.CurrentUser = null;
        Credentials.Token = null;
        FacesContext.getCurrentInstance().getExternalContext().redirect(APP_NAME);
    }

    public void onCheckAuth() throws IOException {
        Response response = userService.login(Credentials.CurrentUser);

        if(response.getStatus() == 200) {
            Credentials.Token = response.getHeaderString("token");
            Credentials.CurrentUser = userService.getMe(Credentials.Token);
            FacesContext.getCurrentInstance().getExternalContext().redirect(APP_NAME);
        } else {
            FacesContext.getCurrentInstance().validationFailed();
        }
    }

    public void onRegisterAuth() {
        if(!registerUser.getPassword().equals(registerUser.getRepeatPassword())) {
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        Response response = userService.register(registerUser);
        if(response.getStatus() != 200)
            FacesContext.getCurrentInstance().validationFailed();
    }

    public void onCancelLogin() {
        Credentials.CurrentUser = null;
        Credentials.Token = null;
    }
}
