package pl.pb.shop_cli.model;

public class UserClaims{
    public Long userID = new Long(0);
    public UserRole userRole = UserRole.NORIGHTS;

    public UserClaims(){}

    public UserClaims(Long userID, String uR){
        this.userID = userID;
        for(UserRole us : UserRole.values()){
            if(us.toString().equals(uR)){
                this.userRole = us;
                break;
            }
        }
    }
}
