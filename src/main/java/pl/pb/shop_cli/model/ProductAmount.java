package pl.pb.shop_cli.model;

public class ProductAmount{
    private long productId;
    private Long amount;

    public ProductAmount() {

    }

    public ProductAmount(long productId, Long amount) {
        this.productId = productId;
        this.amount = amount;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
