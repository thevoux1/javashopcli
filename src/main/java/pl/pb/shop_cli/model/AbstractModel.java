package pl.pb.shop_cli.model;

public class AbstractModel {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
