package pl.pb.shop_cli.model;

import java.math.BigDecimal;

public class ProductOrder extends AbstractModel{
    private Product product;
    private Long amount;

    public ProductOrder(){}
    public ProductOrder(ProductBasket pb){
        this.product = pb.getProduct();
        this.amount = pb.getAmount();
    }

    public ProductOrder(Product p, Long a){
        product = p;
        amount = a;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalPrice() {
        return product.getPrice().multiply(new BigDecimal(amount));
    }
}
