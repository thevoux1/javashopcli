package pl.pb.shop_cli.model;

public enum UserRole {
    NORIGHTS,
    ADMIN,
    CLIENT
}
