package pl.pb.shop_cli.model;

public class User extends AbstractModel {
    private String email;
    private String password;
    private UserRole userRole;
    private String name;
    private String surname;
    private String address;

    public User(String email, String password, UserRole userRole, String name, String surname, String address) {
        this.email = email;
        this.password = password;
        this.userRole = userRole;
        this.name = name;
        this.surname = surname;
        this.address = address;
    }

    public User() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean valid() {
        if(email != null && email != "" && password != null && password != ""
                && name != null && name != "" && surname != null && surname != ""
                && address != null && address != ""){
            return true;

        }
        return false;
    }
}
