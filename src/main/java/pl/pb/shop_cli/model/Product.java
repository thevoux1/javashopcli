package pl.pb.shop_cli.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.pb.shop_cli.exception.ProductDeficitException;

import javax.persistence.Entity;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product extends AbstractModel {
    private String name;
    private BigDecimal price;
    private Long amount;
    private String description;
    private String pictureUrl;

    public Product(String name, BigDecimal price, Long amount, String description, String pictureUrl) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public void updateAmount(Long amount) {
        if(this.amount + amount < 0)
            throw new ProductDeficitException();

        this.amount += amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
