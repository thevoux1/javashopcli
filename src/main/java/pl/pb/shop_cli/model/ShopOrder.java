package pl.pb.shop_cli.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopOrder extends AbstractModel {
    private User user;
    private List<ProductOrder> productOrders;
    private Discount discount;
    private OrderState orderState;

    public ShopOrder(User u, List<ProductOrder> po, Discount d) {
        this.user = u;
        this.productOrders = po;
        this.discount = d;
        this.orderState = OrderState.UNPAID;
    }

    public ShopOrder() {

    }

    public ShopOrder(Basket b) {
        this.user = b.getUser();
        this.discount = b.getDiscount();
        this.orderState = OrderState.UNPAID;
        productOrders = new ArrayList<>();
        for(int i = 0; i<b.getProductBaskets().size();i++){
            this.productOrders.add(new ProductOrder(b.getProductBaskets().get(i)));
        }
    }


    public OrderState getOrderState() {
        return orderState;
    }

    public void updateOrderState() {
        switch(this.orderState) {
            case UNPAID:
                this.orderState = OrderState.PREPARING;
                break;
            case PREPARING:
                this.orderState = OrderState.SENT;
                break;
            case SENT:
                this.orderState = OrderState.DELIVERED;
                break;
        }
    }

    public boolean canNotBePayed() {
        return !orderState.equals(OrderState.UNPAID);
    }

    public boolean canNotBeCancelled() {
        return !orderState.equals(OrderState.UNPAID) && !orderState.equals(OrderState.PREPARING);
    }

    public BigDecimal getTotalPrice() {
        BigDecimal totalPrice = new BigDecimal(0);
        for(ProductOrder po : productOrders)
            totalPrice = totalPrice.add(po.getProduct().getPrice().multiply(new BigDecimal(po.getAmount())));

        if(discount == null)
            return totalPrice;

        return totalPrice.multiply(new BigDecimal(100 - discount.getPercent()).divide(new BigDecimal(100)));
    }

    public void cancelOrder() {
        this.orderState = OrderState.CANCALLED;
    }

    public List<ProductOrder> getProductOrders() {
        return productOrders;
    }

    public void setProductOrders(List<ProductOrder> productOrders) {
        this.productOrders = productOrders;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public void setOrderState(OrderState os){
        this.orderState = os;
    }
}
