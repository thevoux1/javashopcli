package pl.pb.shop_cli.model;

public enum OrderState {
    UNPAID,
    PREPARING,
    SENT,
    DELIVERED,
    CANCALLED;

    OrderState(){}

    public String getTransation() {
        switch (this) {
            case UNPAID:
                return "Nieopłacone";
            case PREPARING:
                return "W trakcie realizacji";
            case SENT:
                return "Wysłane";
            case DELIVERED:
                return "Doręczone";
            case CANCALLED:
                return "Anulowane";
            default:
                return null;
        }
    }
}
