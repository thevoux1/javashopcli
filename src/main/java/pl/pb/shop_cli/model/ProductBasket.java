package pl.pb.shop_cli.model;

import java.math.BigDecimal;

public class ProductBasket extends AbstractModel{
    private Product product;
    private Long amount;

    public ProductBasket(){}

    public ProductBasket(Product p, Long a){
        product = p;
        amount = a;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalPrice() {
        return product.getPrice().multiply(new BigDecimal(amount));
    }
}