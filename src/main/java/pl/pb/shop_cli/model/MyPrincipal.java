package pl.pb.shop_cli.model;

import java.security.Principal;

public class MyPrincipal implements Principal {
    private UserClaims userClaims;

    public MyPrincipal(UserClaims us){
        this.userClaims = us;
    }

    public UserClaims getUserClaims() {
        return userClaims;
    }

    @Override
    public boolean equals(Object another) {
        return false;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String getName() {
        return null;
    }
}
