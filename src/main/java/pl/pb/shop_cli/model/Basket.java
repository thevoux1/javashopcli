package pl.pb.shop_cli.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
public class Basket extends AbstractModel {
    private User user;
    private List<ProductBasket> productBaskets = new ArrayList<>();
    private Discount discount;

    public Basket(User user, Discount discount) {
        this.user = user;
        this.productBaskets = new ArrayList<>();
        this.discount = discount;
    }

    public Basket() {

    }

    public void addProduct(Product p, Long amount){
        if (productBaskets == null ){
            productBaskets = new ArrayList<>();
        }
        productBaskets.add(new ProductBasket(p,amount));
    }

    public void deleteProduct(Product p){
        if (productBaskets != null ){
            for(int i =0; i< productBaskets.size();i++){
                if(productBaskets.get(i).getProduct().getId() == p.getId()){
                    productBaskets.remove(i);
                    return;
                }
            }
        }
    }

    public boolean ifContainProduct(Product p){
        if (productBaskets != null ){
            for(int i =0; i< productBaskets.size();i++){
                if(productBaskets.get(i).getProduct().getId() == p.getId()){
                    return true;
                }
            }
            return false;
        } else {
            productBaskets = new ArrayList<>();
            return false;
        }
    }

    public void changeAmountProduct(Product p, Long newAmount){
        if (productBaskets != null ) {
            for(int i =0; i< productBaskets.size();i++){
                if(productBaskets.get(i).getProduct().getId() == p.getId()){
                    productBaskets.remove(i);
                    productBaskets.add(i,new ProductBasket(p,newAmount));
                    return;
                }
            }
        } else {
            productBaskets = new ArrayList<>();
        }
    }

    public void clearBasket(){
        this.productBaskets = new ArrayList<>();
        this.discount = null;
    }

    public User getUser() {
        return user;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public List<ProductBasket> getProductBaskets() {
        return productBaskets;
    }

    public void setProductBaskets(List<ProductBasket> productBaskets) {
        this.productBaskets = productBaskets;
    }
}
