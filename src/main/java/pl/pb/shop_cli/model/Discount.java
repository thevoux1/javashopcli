package pl.pb.shop_cli.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Discount extends AbstractModel {
    private String code;
    private byte percent;

    public Discount(String code, byte percent) {
        this.code = code;
        this.percent = percent;
    }

    public Discount() {

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public byte getPercent() {
        return percent;
    }

    public void setPercent(byte percent) {
        this.percent = percent;
    }
}
