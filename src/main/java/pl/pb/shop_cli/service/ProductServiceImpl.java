package pl.pb.shop_cli.service;

import pl.pb.shop_cli.dao.ProductDao;
import pl.pb.shop_cli.model.Product;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class ProductServiceImpl implements ProductService {
    @EJB
    private ProductDao productDao;

    @Override
    public List<Product> findAll() {
        return productDao.findAll();
    }

    @Override
    public Product save(Product product, String token) {
        return productDao.save(product, token);
    }

    @Override
    public void remove(Product product, String token) {
        productDao.remove(product, token);
    }
}
