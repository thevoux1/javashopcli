package pl.pb.shop_cli.service;

import pl.pb.shop_cli.dao.UserDao;
import pl.pb.shop_cli.model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

@Stateless
public class UserServiceImpl implements UserService {
    @EJB
    private UserDao userDao;

    @Override
    public Response login(User user) {
        return userDao.login(user);
    }

    @Override
    public Response register(User user) {
        return userDao.register(user);
    }

    @Override
    public User getMe(String token) {
        return userDao.getMe(token);
    }
}
