package pl.pb.shop_cli.service;

import pl.pb.shop_cli.dao.OrderDao;
import pl.pb.shop_cli.model.OrderState;
import pl.pb.shop_cli.model.ShopOrder;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
public class OrderServiceImpl implements OrderService {
    @EJB
    private OrderDao orderDao;

    @Override
    public List<ShopOrder> findAllOrders(String token) {
        return orderDao.findAllOrders(token);
    }

    @Override
    public List<ShopOrder> findMyOrders(String token) {
        return orderDao.findMyOrders(token);
    }

    @Override
    public Response submitOrder(String token) {
        return orderDao.submitOrder(token);
    }

    @Override
    public Response payOrder(ShopOrder order, String token) {
        return orderDao.payOrder(order, token);
    }

    @Override
    public Response setOrderState(ShopOrder order, OrderState orderState, String token) {
        return orderDao.setOrderState(order, orderState, token);
    }
}
