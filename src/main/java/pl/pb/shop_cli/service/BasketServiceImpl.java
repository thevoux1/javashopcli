package pl.pb.shop_cli.service;

import pl.pb.shop_cli.dao.BasketDao;
import pl.pb.shop_cli.model.Basket;
import pl.pb.shop_cli.model.ProductAmount;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

@Stateless
public class BasketServiceImpl implements BasketService {
    @EJB
    private BasketDao basketDao;

    @Override
    public Basket getBasket(String token) {
        return basketDao.getBasket(token);
    }

    @Override
    public Response addProduct(ProductAmount productAmount, String token) {
        return basketDao.addProduct(productAmount, token);
    }

    @Override
    public void removeProduct(Long productId, String token) {
        basketDao.removeProduct(productId, token);
    }

    @Override
    public Response changeProductAmount(ProductAmount productAmount, String token) {
        return basketDao.changeProductAmount(productAmount, token);
    }

    @Override
    public Response addDiscount(String code, String token) {
        return basketDao.addDiscount(code, token);
    }

    @Override
    public Response removeDiscount(String token) {
        return basketDao.removeDiscount(token);
    }

    @Override
    public void clearBasket(String token) {
        basketDao.clearBasket(token);
    }
}
