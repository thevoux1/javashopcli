package pl.pb.shop_cli.service;

import pl.pb.shop_cli.model.OrderState;
import pl.pb.shop_cli.model.ShopOrder;

import javax.ws.rs.core.Response;
import java.util.List;

public interface OrderService {
    List<ShopOrder> findAllOrders(String token);
    List<ShopOrder> findMyOrders(String token);
    Response submitOrder(String token);
    Response payOrder(ShopOrder order, String token);
    Response setOrderState(ShopOrder order, OrderState orderState, String token);
}
