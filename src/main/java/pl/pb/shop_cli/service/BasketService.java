package pl.pb.shop_cli.service;

import pl.pb.shop_cli.model.Basket;
import pl.pb.shop_cli.model.ProductAmount;

import javax.ws.rs.core.Response;

public interface BasketService {
    Basket getBasket(String token);
    Response addProduct(ProductAmount productAmount, String token);
    void removeProduct(Long productId, String token);
    Response changeProductAmount(ProductAmount productAmount, String token);
    Response addDiscount(String code, String token);
    Response removeDiscount(String token);
    void clearBasket(String token);
}
