package pl.pb.shop_cli.service;

import pl.pb.shop_cli.dao.DiscountDao;
import pl.pb.shop_cli.model.Discount;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class DiscountServiceImpl implements DiscountService {
    @EJB
    private DiscountDao discountDao;

    @Override
    public List<Discount> findAll(String token) {
        return discountDao.findAll(token);
    }

    @Override
    public Discount findByCode(String code, String token) {
        return discountDao.findByCode(code, token);
    }

    @Override
    public Discount save(Discount discount, String token) {
        return discountDao.save(discount, token);
    }

    @Override
    public void remove(Discount discount, String token) {
        discountDao.remove(discount, token);
    }
}
