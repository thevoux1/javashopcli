package pl.pb.shop_cli.service;

import pl.pb.shop_cli.model.User;

import javax.ws.rs.core.Response;

public interface UserService {
    Response login(User user);
    Response register(User user);
    User getMe(String token);
}
