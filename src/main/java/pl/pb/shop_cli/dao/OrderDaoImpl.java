package pl.pb.shop_cli.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.pb.shop_cli.model.OrderState;
import pl.pb.shop_cli.model.ShopOrder;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

import static pl.pb.shop_cli.Constants.GLOBAL_URI;

@Stateless
public class OrderDaoImpl implements OrderDao {

    @Override
    public List<ShopOrder> findAllOrders(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "order/all").port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).get();
        String json = response.readEntity(String.class);

        try {
            return new ObjectMapper().readValue(json, new TypeReference<List<ShopOrder>>(){});
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Override
    public List<ShopOrder> findMyOrders(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "order").port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).get();
        String json = response.readEntity(String.class);

        try {
            return new ObjectMapper().readValue(json, new TypeReference<List<ShopOrder>>(){});
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Override
    public Response submitOrder(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "order/submit").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).put(Entity.entity("{}", MediaType.APPLICATION_JSON));
    }

    @Override
    public Response payOrder(ShopOrder order, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "order/pay/" + order.getId()).port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).put(Entity.entity("{}", MediaType.APPLICATION_JSON));
    }

    @Override
    public Response setOrderState(ShopOrder order, OrderState orderState, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "order/" + order.getId() + "/" + orderState).port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).put(Entity.entity("{}", MediaType.APPLICATION_JSON));
    }
}
