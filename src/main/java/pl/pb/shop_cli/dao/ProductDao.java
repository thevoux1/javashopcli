package pl.pb.shop_cli.dao;

import pl.pb.shop_cli.model.Product;

import java.util.List;

public interface ProductDao {
    List<Product> findAll();
    Product save(Product product, String token);
    void remove(Product product, String token);
}
