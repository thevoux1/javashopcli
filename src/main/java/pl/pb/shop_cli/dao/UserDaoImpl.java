package pl.pb.shop_cli.dao;

import pl.pb.shop_cli.model.User;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static pl.pb.shop_cli.Constants.GLOBAL_URI;

@Stateless
public class UserDaoImpl implements UserDao {

    @Override
    public Response login(User user) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "auth/login").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    @Override
    public Response register(User user) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "auth/register").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    @Override
    public User getMe(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "user").port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).get();
        return response.readEntity(User.class);
    }
}
