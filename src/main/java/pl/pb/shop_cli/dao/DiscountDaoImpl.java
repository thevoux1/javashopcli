package pl.pb.shop_cli.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.pb.shop_cli.model.Discount;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

import static pl.pb.shop_cli.Constants.GLOBAL_URI;

@Stateless
public class DiscountDaoImpl implements DiscountDao {

    @Override
    public List<Discount> findAll(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "discount").port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).get();
        String json = response.readEntity(String.class);

        try {
            return new ObjectMapper().readValue(json, new TypeReference<List<Discount>>(){});
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Override
    public Discount findByCode(String code, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "discount/" + code).port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).get();
        return response.readEntity(Discount.class);
    }

    @Override
    public Discount save(Discount discount, String token) {
        if(discount.getId() == null) {
            URI uri = UriBuilder.fromUri(GLOBAL_URI + "discount").port(8080).build();
            Client client = ClientBuilder.newClient();
            Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).post(Entity.entity(discount, MediaType.APPLICATION_JSON));
            return response.readEntity(Discount.class);
        } else {
            URI uri = UriBuilder.fromUri(GLOBAL_URI + "discount/" + discount.getId()).port(8080).build();
            Client client = ClientBuilder.newClient();
            Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).put(Entity.entity(discount, MediaType.APPLICATION_JSON));
            return response.readEntity(Discount.class);
        }
    }

    @Override
    public void remove(Discount discount, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "discount/" + discount.getId()).port(8080).build();
        Client client = ClientBuilder.newClient();
        client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).delete();
    }
}
