package pl.pb.shop_cli.dao;

import pl.pb.shop_cli.model.Basket;
import pl.pb.shop_cli.model.Product;
import pl.pb.shop_cli.model.ProductAmount;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static pl.pb.shop_cli.Constants.GLOBAL_URI;

@Stateless
public class BasketDaoImpl implements BasketDao {

    @Override
    public Basket getBasket(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket").port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).get();
        return response.readEntity(Basket.class);
    }

    @Override
    public Response addProduct(ProductAmount productAmount, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket/add").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).post(Entity.entity(productAmount, MediaType.APPLICATION_JSON));
    }

    @Override
    public void removeProduct(Long productId, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket/delete").port(8080).build();
        Client client = ClientBuilder.newClient();
        client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).post(Entity.entity(productId, MediaType.APPLICATION_JSON));
    }

    @Override
    public Response changeProductAmount(ProductAmount productAmount, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket/change").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).post(Entity.entity(productAmount, MediaType.APPLICATION_JSON));
    }

    @Override
    public Response addDiscount(String code, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket/discount").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).post(Entity.entity(code, MediaType.APPLICATION_JSON));
    }

    @Override
    public Response removeDiscount(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket/discount").port(8080).build();
        Client client = ClientBuilder.newClient();
        return client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).delete();
    }

    @Override
    public void clearBasket(String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "basket/clear").port(8080).build();
        Client client = ClientBuilder.newClient();
        client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).delete();
    }
}
