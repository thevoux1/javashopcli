package pl.pb.shop_cli.dao;

import pl.pb.shop_cli.model.User;

import javax.ws.rs.core.Response;

public interface UserDao {
    Response login(User user);
    Response register(User user);
    User getMe(String token);
}
