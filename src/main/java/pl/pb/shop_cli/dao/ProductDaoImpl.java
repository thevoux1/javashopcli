package pl.pb.shop_cli.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.pb.shop_cli.model.Product;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

import static pl.pb.shop_cli.Constants.GLOBAL_URI;

@Stateless
public class ProductDaoImpl implements ProductDao {

    @Override
    public List<Product> findAll() {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "product").port(8080).build();
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).request().get();
        String json = response.readEntity(String.class);

        try {
            return new ObjectMapper().readValue(json, new TypeReference<List<Product>>(){});
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Override
    public Product save(Product product, String token) {

        if(product.getId() == null) {
            URI uri = UriBuilder.fromUri(GLOBAL_URI + "product").port(8080).build();
            Client client = ClientBuilder.newClient();
            Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).post(Entity.entity(product, MediaType.APPLICATION_JSON));
            return response.readEntity(Product.class);
        } else {
            URI uri = UriBuilder.fromUri(GLOBAL_URI + "product/" + product.getId()).port(8080).build();
            Client client = ClientBuilder.newClient();
            Response response = client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).put(Entity.entity(product, MediaType.APPLICATION_JSON));
            return response.readEntity(Product.class);
        }
    }

    @Override
    public void remove(Product product, String token) {
        URI uri = UriBuilder.fromUri(GLOBAL_URI + "product/" + product.getId()).port(8080).build();
        Client client = ClientBuilder.newClient();
        client.target(uri).request().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).delete();
    }
}
