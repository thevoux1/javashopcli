package pl.pb.shop_cli.dao;

import pl.pb.shop_cli.model.Discount;

import java.util.List;

public interface DiscountDao {
    List<Discount> findAll(String token);
    Discount findByCode(String code, String token);
    Discount save(Discount discount, String token);
    void remove(Discount discount, String token);
}
